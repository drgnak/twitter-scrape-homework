import tweet_sentiment

def count_terms(tweet_list):
    """Count each term in list and return dictionary of term counts"""
    term_count = {}
    for tweet in tweet_list:
        text_list = tweet_sentiment.raw_text_to_string(tweet['text']).split()
        #Add terms to term count
        for term in text_list:
            try:
                term_count[term] += 1
            except KeyError:
                term_count[term] = 1
    return term_count

def print_term_freq(terms):
    """Return frequency of terms"""
    total_terms = sum(terms.values())
    for word in terms:
        frequency = terms[word]/total_terms
        print(word, '%.3f' % frequency)
    

if __name__ == '__main__':
    tweet_list = tweet_sentiment.load_tweet_list('output.txt', 20)
    term_count = count_terms(tweet_list)
    print_term_freq(term_count)
    
