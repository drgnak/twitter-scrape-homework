import tweet_sentiment

def count_hashtags(tweet_list):
    """Count each keyword in tweet and return dictionary of term counts"""
    hashtag_count = {}
    for tweet in tweet_list:
        text_list = tweet_sentiment.raw_text_to_string(tweet['text']).split()
        #Add terms to term count
        for term in text_list:
            try:
                hashtag_count[term] += 1
            except KeyError:
                hashtag_count[term] = 1
    return hashtag_count

def print_term_freq(terms):
    """Print frequency of keywords"""
    total_terms = sum(terms.values())
    for word in terms:
        frequency = terms[word]/total_terms
        print(word, '%.3f' % frequency)
    

if __name__ == '__main__':
    tweet_list = tweet_sentiment.load_tweet_list('output.txt')
    #term_count = count_terms(tweet_list)
    #print_term_freq(term_count)
    for tweet in tweet_list:
        hashtags = tweet['entities']['hashtags']
        for hashtag in hashtags:
            print(hashtag['text'])
    
