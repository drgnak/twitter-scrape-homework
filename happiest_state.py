import tweet_sentiment

states = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY',
}

def get_state_abbr(name):
    """Get state abbreviation from full name"""
    state = ''
    
    split_name = name.split(', ')
    try:
        if split_name[1] == 'USA':
            state = states[split_name[0]]
        else:
            state = split_name[1]
    except KeyError:
        print("State does not exist")

    return state

def dict_to_list(dictionary):
    """convert dict to list of tuples"""
    converted = [(key,value) for key, value in dictionary.items()]
    return converted

def sort_state_sentiment(state_sentiment):
    """sort by highest sentiment first"""
    return sorted(state_sentiment, reverse=True, key=lambda x:(x[1]))
    

if __name__ == '__main__':
    affinity_scores = tweet_sentiment.load_affinity('AFINN-111.txt')
    tweet_list = tweet_sentiment.load_tweet_list('output.txt')

    state_sentiment = {}
    for tweet in tweet_list:
        place = tweet["place"]
        try:
            if place['country_code'] == 'US':
                state = get_state_abbr(place['full_name'])
                try:
                    sentiment = tweet_sentiment.compute_score(
                            tweet['text'], affinity_scores)
                except KeyError:
                    #print('No text field')
                    pass
                try:
                    state_sentiment[state] += sentiment
                except KeyError:
                    state_sentiment[state] = sentiment     
        except TypeError:
            #print("No place data")
            pass
    print(sort_state_sentiment(dict_to_list(state_sentiment)))
