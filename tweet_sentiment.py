import json

def load_tweet_list(json_file = '', num_tweet_list = -1):
    """Load a number of tweet_list with 'lang' == 'en' and 'text' field from json_file"""
    f = open(json_file)
    tweet_list = []
    while len(tweet_list) < num_tweet_list or num_tweet_list == -1:
        try:
            tweet = json.loads(json.loads(f.readline()))
            try:
                if tweet['lang'] == 'en' and 'text' in tweet:
                    tweet_list.append(tweet)
            except KeyError:
                #silently pass
                pass
        except ValueError:
            #no more lines available in file
            print("No more lines available in file. Continuing with %d tweets."
                  % len(tweet_list))
            break
    return tweet_list

def raw_text_to_string(raw_text = ''):
    """Converts raw text from a twitter post to a printable string"""
    return raw_text.encode('utf-8','ignore').decode('ascii','ignore')

def compute_score(text = '', scores = {}):
    """Compute sentiment score from text and affinity scores"""
    score = 0
    #print(tweet.split())

    #Compute scores for each word
    for word in text.split():
        try:
            score += scores[word]
        except KeyError:
            #No change to score if word does not exist
            pass
    return score

def load_affinity(file = ''):
    """create a dictionary with affinity scores"""
    f = open(file)
    scores = {}
    for line in f:
        term, score = line.split('\t')
        scores[term] = int(score)
    return scores

if __name__ == '__main__':
    affinity_scores = load_affinity('AFINN-111.txt')
    tweet_list = load_tweet_list('output.txt', 20)

    for tweet in tweet_list:
        print('Tweet:', raw_text_to_string(tweet['text']))
        print('Sentiment score:', compute_score(
            raw_text_to_string(tweet['text']), affinity_scores), '\n')
